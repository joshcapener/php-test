<?php

use PokePHP\PokeApi;

require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';


$search = $_GET['search'];
$begin = $_GET['begin'] ?? 1;
$end = $_GET['end'] ?? 12;
$client = new PokeApi;
$data = !empty($search) ? [$search] : range($begin, $end);
$searchData = [];
$cacheDir = ".." . DIRECTORY_SEPARATOR . "pokemon_cache" . DIRECTORY_SEPARATOR;
foreach ($data as $query) {
    $cacheFile = $cacheDir . "*-{$query}-*.pc";
    if ($matches = glob($cacheFile)) {
        $pokemon = json_decode(file_get_contents(reset($matches)));
    } else {
        $rawPokemon = $client->pokemon(strtolower($query));
        $pokemon = json_decode($rawPokemon);
        if (is_object($pokemon)) {
            file_put_contents($cacheDir . "P-{$pokemon->name}-{$pokemon->id}-M.pc", $rawPokemon);
        }
    }
    if (!is_object($pokemon)) {
        continue;
    }
    $abilities = [];
    foreach ($pokemon->abilities as $ability) {
        $abilities[] = $ability->ability->name . ($ability->is_hidden ? ' (hidden)' : '');
    }
    $searchData[] = [
        'name' => ucfirst($pokemon->name) . " ({$pokemon->id})",
        'height' => round($pokemon->height / 10, 2),
        'weight' => round($pokemon->weight / 10, 2),
        'image' => $pokemon->sprites->front_default,
        'species' => $pokemon->species->name,
        'abilities' => $abilities,
    ];

}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Pokédex</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>

<div class="row">
    <div class="col mb-2 col-md-4 offset-md-4">
        <p class="text-center">Enter a Pokemon's Name or Id and press Enter</p>
        <form method="get" action="/">
            <div class="form-row">
                <div class="col">
                    <input class="form-control" name="search">
                </div>
                <div class="col-md-3 col">
                    <input type="submit" class="btn btn-primary" value="Search">
                </div>

            </div>
        </form>

    </div>
</div>
<div id="pokedex">
    <div class="container">
        <?php if ($searchData): ?>
            <pokedex-entry :pokemon-list='<?= json_encode($searchData); ?>'></pokedex-entry>
        <?php else: ?>
            <div class="alert alert-danger">There was no results matching "<?= htmlspecialchars($search); ?>"</div>
        <?php endif; ?>
        <?php if (count($data) > 1): ?>
            <?php if ($begin > 1): ?>
                <a class="btn btn-secondary" href="/?begin=<?= $begin - 12; ?>&end=<?= $begin - 1; ?>">Previous Page</a>
            <?php endif ?>
            <a class="btn btn-primary" href="/?begin=<?= $end + 1; ?>&end=<?= $end + 12; ?>">Next Page</a>
        <?php endif ?>
    </div>

</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script src="js/pokedex.js"></script>
</body>
</html>